/**
 * @type {import('next').NextConfig}
 */
module.exports = {
  reactStrictMode: true,
  publicRuntimeConfig: {
    clientName: process.env.CLIENT_NAME,
  },
  serverRuntimeConfig: {
  },
}
