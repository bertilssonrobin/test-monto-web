import { Button } from '~/components/Button'
import type { NextPage } from 'next'

const Home: NextPage = () => {
  return (
    <div className="container mx-auto my-12">
      <div className="py-56 text-center bg-gray-300 rounded-lg">
        <h1 className="text-6xl font-semibold">
          The future of lending, today.
        </h1>

        <div className="mt-8">
          <Button>
            Get Started
          </Button>
        </div>
      </div>
    </div>
  )
}

export default Home
