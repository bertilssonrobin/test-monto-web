import { FunctionComponent } from 'react'

type ButtonProps = {}

export const Button: FunctionComponent<ButtonProps> = ({ children }) => {
  return (
    <button className="px-6 py-4 text-white rounded-full bg-primary">
      {children}
    </button>
  )
}
